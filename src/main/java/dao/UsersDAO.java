package dao;

import database.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.User;
import org.mindrot.jbcrypt.BCrypt;
public class UsersDAO {
    
    public static String hashPassword(String passwordPlain){
        String hashedPassword = BCrypt.hashpw(passwordPlain, BCrypt.gensalt());
        return hashedPassword;
    }
    
    public static boolean verifyPassword(String passwordPlain, String hashedPassword){
        return BCrypt.checkpw(passwordPlain, hashedPassword);
    }
    
    public static void createUserDAO(User user){
        try(Connection connection = DbConnection.get_Connection()){
            PreparedStatement stmt = null;
            try{
                String query = "INSERT INTO users(name_user,password_user) "
                             + "VALUES(?,?)";
                stmt = connection.prepareStatement(query);
                stmt.setString(1, user.getName_user());
                stmt.setString(2, user.getPassword_user());
                stmt.executeUpdate();
                System.out.println("User created.");
            }catch(SQLException ex){
                System.out.println(ex.getMessage());
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static String listUsersDAO(){
        String listUsers = "";
        try(Connection connection = DbConnection.get_Connection()){
            PreparedStatement stmt = null;
            ResultSet rs = null;
            try{
                String query = "SELECT id_user, name_user "
                              + "FROM users";
                stmt = connection.prepareStatement(query);
                rs = stmt.executeQuery();
                
                while(rs.next()){
                    listUsers += "ID: " + rs.getInt("id_user") + ".\n";
                    listUsers += "Name: " + rs.getString("name_user") + ".\n";
                }
                
            }catch(SQLException ex){
                System.out.println(ex.getMessage());
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return listUsers;
    }
    
    public static void deleteUserDAO(int id){
        try(Connection connection = DbConnection.get_Connection()){
            PreparedStatement stmt = null;
            try{
                String query = "DELETE "
                             + "FROM users "
                             + "WHERE users.id_user = ?";
                stmt = connection.prepareStatement(query);
                stmt.setInt(1, id);
                stmt.executeUpdate();
                System.out.println("User deleted.");
            }catch(SQLException ex){
                System.out.println(ex.getMessage());
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static void udapteUserDAO(User user){
        try(Connection connection = DbConnection.get_Connection()){
            PreparedStatement stmt = null;
            try{
                String query = "UPDATE users"
                             + "SET name_user = ?, password_user = ?";
                stmt = connection.prepareStatement(query);
                stmt.setString(1, user.getName_user());
                stmt.setString(2, user.getPassword_user());
                stmt.executeUpdate();
                System.out.println("Successful update.");
            }catch(SQLException ex){
                System.out.println(ex.getMessage());
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static User loginUser(User user){
        try(Connection connection = DbConnection.get_Connection()){
            PreparedStatement stmt = null;
            ResultSet rs = null;
            try{
                String query = "SELECT id_user, name_user, password_user "
                             + "FROM users "
                             + "WHERE users.name_user = ?";
                stmt = connection.prepareStatement(query);
                stmt.setString(1, user.getName_user());
                rs = stmt.executeQuery();
                if(rs.next() && verifyPassword(user.getPassword_user(), 
                        rs.getString("password_user"))){
                    User newUser = new User();
                    System.out.println("Login successful.");
                    newUser.setUser_id(rs.getInt("id_user"));
                    newUser.setName_user(rs.getString("name_user"));
                    return newUser;
                }
            }catch(SQLException ex){
                System.out.println(ex.getMessage());
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return null;
    }
    
}
