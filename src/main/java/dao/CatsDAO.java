package dao;

import database.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.CatsFav;

public class CatsDAO {
    
    public static void createCatDAO(CatsFav cat){
        try(Connection connection = DbConnection.get_Connection()){
            PreparedStatement stmt = null;
            try{
                String query = "INSERT INTO imagecats(id_cat, id_image, sub_id, created_at, url) "
                             + "VALUES (?,?,?,?,?)";
                stmt.setString(1,cat.getId());
                stmt.setString(2, cat.getImage_id());
                stmt.setString(3,cat.getSub_id());
                stmt.setString(4, cat.getCreated_at());
                stmt.setString(5, cat.getImage().getUrl());
                stmt = connection.prepareStatement(query);
                stmt.executeUpdate();
                System.out.println("ImageCat created.");
            }catch(SQLException ex){
                System.out.println(ex.getMessage());
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static String listCats(){
        String listCats = "";
        try(Connection connection = DbConnection.get_Connection()){
            PreparedStatement stmt = null;
            ResultSet rs = null;
            try{
                String query = "SELECT * "
                             + "FROM imagecats ";
                stmt = connection.prepareStatement(query);
                rs = stmt.executeQuery();
                
                while(rs.next()){
                    listCats += "ID: " + rs.getString("id_cat") + ".\n";
                    listCats += "ID Image: " + rs.getString("id_image") + ".\n";
                    listCats += "Sub ID: " + rs.getString("sub_id") + ".\n";
                    listCats += "Created at: " + rs.getString("created_at") + ".\n";
                    listCats += "URL: " + rs.getString("url") + ".\n";
                }
            }catch(SQLException ex){
                System.out.println(ex.getMessage());
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return listCats;
    }
    
    public static void deleteCatDAO(String id){
        try(Connection connection = DbConnection.get_Connection()){
            PreparedStatement stmt = null;
            try{
                String query = "DELETE "
                             + "FROM imagecats "
                             + "WHERE id_cat = ?";
                stmt = connection.prepareStatement(query);
                stmt.setString(1, id);
                stmt.executeUpdate();
                System.out.println("Cat deleted");
            }catch(SQLException ex){
                System.out.println(ex.getMessage());
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static void updateCatDAO(CatsFav cat){
        try(Connection connection = DbConnection.get_Connection()){
            PreparedStatement stmt = null;
            try{
                String query = "UPDATE "
                             + "SET sub_id = ? "
                             + "WHERE id_cat = ?";
                stmt = connection.prepareStatement(query);
                stmt.setString(1, cat.getSub_id());
                stmt.setString(2, cat.getId());
                stmt.executeUpdate();
                System.out.println("Successful update.");
            }catch(SQLException ex){
                System.out.println(ex.getMessage());
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
}
