package service;

import dao.UsersDAO;
import model.User;

public class UserService {
    
    public static void createUser(String name, String password){
        User user = new User();
        user.setName_user(name);
        String passwordHashed = UsersDAO.hashPassword(password);
        user.setPassword_user(passwordHashed);
        UsersDAO.createUserDAO(user);
    }
    
    public static User loginUser(String name, String password){
        User user = new User();
        user.setName_user(name);
        user.setPassword_user(password);
        return UsersDAO.loginUser(user);
    }
}
