package service;

import com.google.gson.Gson;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Cat;
import model.CatsFav;
import model.User;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CatService {

    public static Cat viewCat() throws IOException {
        // Se crea un cliente HTTP utilizando OkHttpClient
        OkHttpClient client = new OkHttpClient().newBuilder().build();

        // Se define el tipo de medio y el cuerpo de la solicitud (en este caso, no hay cuerpo)
        MediaType mediaType = MediaType.parse("text/plain");
        RequestBody body = RequestBody.create(mediaType, "");

        // Se construye la solicitud GET para obtener una imagen de gato aleatoria desde la API
        Request request = new Request.Builder()
                .url("https://api.thecatapi.com/v1/images/search")
                .method("GET", null)
                .build();

        // Se ejecuta la solicitud y se obtiene la respuesta
        Response response = client.newCall(request).execute();

        // Se convierte la respuesta de la API en una cadena de texto
        String eJson = response.body().string();

        // La respuesta de la API trae un corchete al inicio y al final, se deben eliminar
        eJson = eJson.substring(1,eJson.length());
        eJson = eJson.substring(0,eJson.length()-1);

        // Se crea un objeto de tipo Gson para serializar/deserializar JSON
        Gson gson = new Gson();

        // Se convierte la cadena JSON en un objeto de tipo Cat utilizando Gson
        Cat cat = gson.fromJson(eJson, Cat.class);
        return cat;
    }
    
    public static void setFavorite(Cat cat,User user)throws IOException{
        OkHttpClient client = new OkHttpClient().newBuilder().build();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, "{\r\n    \"image_id\":\""+cat.getId()+"\",\r\n    \"sub_id\":\""+user.getUser_id()+"\"\r\n}");
        Request request = new Request.Builder()
          .url("https://api.thecatapi.com/v1/favourites")
          .method("POST", body)
          .addHeader("x-api-key", "live_46I4D9u6wjWlGhqxvDyVtzjPA4EhsvIYauS4o5Syio5F5qJjTT6VGdoZXpbrnQkU")
          .addHeader("Content-Type", "application/json")
          .build();
        Response response = client.newCall(request).execute();
    }
    
    public static CatsFav[] getFavorites(){
        try {
            OkHttpClient client = new OkHttpClient().newBuilder().build();
            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = RequestBody.create(mediaType, "");
            Request request = new Request.Builder()
                    .url("https://api.thecatapi.com/v1/favourites")
                    .method("GET", null)
                    .addHeader("x-api-key", "live_46I4D9u6wjWlGhqxvDyVtzjPA4EhsvIYauS4o5Syio5F5qJjTT6VGdoZXpbrnQkU")
                    .build();
            Response response = client.newCall(request).execute();
            String eJson = response.body().string();
            Gson gson = new Gson();
            CatsFav[] catsFav = gson.fromJson(eJson, CatsFav[].class);
            
            return catsFav;
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
}
