package model;

public class User {
    private int user_id;
    private String name_user;
    private String password_user;

    public User() {}
    
    public User(String name_user, String password_user) {
        this.name_user = name_user;
        this.password_user = password_user;
    }
    
    public String getName_user() {
        return name_user;
    }

    public void setName_user(String name_user) {
        this.name_user = name_user;
    }

    public String getPassword_user() {
        return password_user;
    }

    public void setPassword_user(String password_user) {
        this.password_user = password_user;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
    
}
