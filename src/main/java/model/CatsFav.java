package model;

public class CatsFav {
    private String id;
    private String user_id;
    private String image_id;
    private String sub_id;
    private String created_at;
    private CatImage image;
    
    public CatsFav(){}

    public CatsFav(String id, String user_id, String image_id, String sub_id, String created_at, CatImage image) {
        this.id = id;
        this.user_id = user_id;
        this.image_id = image_id;
        this.sub_id = sub_id;
        this.created_at = created_at;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage_id() {
        return image_id;
    }

    public void setImage_id(String image_id) {
        this.image_id = image_id;
    }

    public CatImage getImage() {
        return this.image;
    }

    public void setImage(CatImage image) {
        this.image = image;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getSub_id() {
        return sub_id;
    }

    public void setSub_id(String sub_id) {
        this.sub_id = sub_id;
    }
    
    public static class CatImage{
        String url;
        String id;
        
        public CatImage(){}
        
        public CatImage(String url, String id){
            this.url = url;
            this.id = id;
        }
        
        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

    } 
    
}
