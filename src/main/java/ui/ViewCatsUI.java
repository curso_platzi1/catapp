/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package ui;

import com.formdev.flatlaf.FlatDarkLaf;
import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import model.Cat;
import model.User;
import service.CatService;

public class ViewCatsUI extends javax.swing.JFrame {

    public ViewCatsUI() {
        initComponents();
        updateLabel(jLabel1);
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        myFavorites = new javax.swing.JButton();
        favorite = new javax.swing.JButton();
        next = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setPreferredSize(new java.awt.Dimension(600, 450));

        myFavorites.setText("My Favorites");
        myFavorites.setPreferredSize(new java.awt.Dimension(190, 50));
        myFavorites.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                myFavoritesActionPerformed(evt);
            }
        });

        favorite.setText("Favorite");
        favorite.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                favoriteActionPerformed(evt);
            }
        });

        next.setText("Next");
        next.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(myFavorites, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(favorite, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(next, javax.swing.GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 388, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(myFavorites, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(favorite, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(next, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void myFavoritesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_myFavoritesActionPerformed
        ViewCatsFavUI viewCatsFavUI = new ViewCatsFavUI();
        viewCatsFavUI.setVisible(true);
    }//GEN-LAST:event_myFavoritesActionPerformed

    private void nextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextActionPerformed
        updateLabel(jLabel1);
    }//GEN-LAST:event_nextActionPerformed

    private void favoriteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_favoriteActionPerformed
        ViewCatsUI.setFavoriteCat(globalCat,globalUser);
    }//GEN-LAST:event_favoriteActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]){
        
        FlatDarkLaf.setup();
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ViewCatsUI().setVisible(true);
            }
        });
    }
    
    private void updateLabel(javax.swing.JLabel jlabel){
        try{
            Cat newCat = CatService.viewCat();
            URL imageUrl = new URL(newCat.getUrl());
            
            //Cargo la imagen desde la URL
            Image image = ImageIO.read(imageUrl);
            
            //Escalo la imagen al tamaño del label
            int labelWidth = jlabel.getWidth();
            int labelHeight = jlabel.getHeight();
            Image scaledImage = image.getScaledInstance(labelWidth, labelHeight, Image.SCALE_SMOOTH);
            
            //Creo el imageIcon a partir de la imagen escalada
            ImageIcon catImageIcon = new ImageIcon(scaledImage);
            
            //Asigno la icon al label
            jlabel.setIcon(catImageIcon);
            globalCat = newCat;
            
            }catch(IOException e){
                e.printStackTrace();
            }
        
    }
    
    public static void setFavoriteCat(Cat cat, User user){
        
        try{
            if(globalCat != null){
            CatService.setFavorite(cat,user);
            }else{
                System.out.println("No se guardo el gato favorito.");
            }
        }catch(IOException e){
            e.printStackTrace();
        }
        
    }
    
    public Cat getGlobalCat(){
        return ViewCatsUI.globalCat;
    }
    
    public static User getGlobalUser(){
        return ViewCatsUI.globalUser;
    }
    
    public static void setGlobalUser(User globalUser){
        ViewCatsUI.globalUser = globalUser;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton favorite;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton myFavorites;
    private javax.swing.JButton next;
    // End of variables declaration//GEN-END:variables

    private static Cat globalCat;
    private static User globalUser;
}
