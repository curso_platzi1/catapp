package ui;

import com.formdev.flatlaf.FlatDarkLaf;
import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import model.CatsFav;
import model.User;
import service.CatService;

public class ViewCatsFavUI extends javax.swing.JFrame {

    /**
     * Creates new form ViewCatsFavUI
     */
    public ViewCatsFavUI() {
        initComponents();
        
        if (cat == null || cat.length == 0) {
            // En este punto, cat es null o está vacío, por lo que no inicializamos el JFrame
            // Puedes mostrar un mensaje o realizar alguna otra acción si lo deseas
            System.out.println("No hay datos para mostrar.");
            this.dispose();
        }
        
        updateFavContainer();
        updateFavoriteUI(posImage);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelImage = new javax.swing.JPanel();
        catImage = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setSize(new java.awt.Dimension(600, 450));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        javax.swing.GroupLayout panelImageLayout = new javax.swing.GroupLayout(panelImage);
        panelImage.setLayout(panelImageLayout);
        panelImageLayout.setHorizontalGroup(
            panelImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(catImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        panelImageLayout.setVerticalGroup(
            panelImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(catImage, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
        );

        jButton1.setText("Previus");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Next");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, 294, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
       if(((this.posImage + 1) < cat.length) && ((this.posImage + 1) >= 0)){
            this.posImage += 1;
            updateFavoriteUI(posImage);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if(((this.posImage - 1) < cat.length) && ((this.posImage - 1) >= 0)){
            this.posImage -= 1;
            updateFavoriteUI(posImage);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        
    }//GEN-LAST:event_formWindowClosing
    
    public static void main(String args[]) {
        FlatDarkLaf.setup();
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ViewCatsFavUI().setVisible(true);
            }
        });
    }
    
    private void updateFavoriteUI(int posImage) {
        this.posImage = posImage;
        try{
            URL url = new URL(cat[posImage].getImage().getUrl()); 
            Image image = ImageIO.read(url);
            Image imageScaled = image.getScaledInstance(catImage.getWidth(),
                    catImage.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon imageIcon = new ImageIcon(imageScaled);
            catImage.setIcon(imageIcon);

        }catch(IOException e){
            System.out.println(e.getMessage());
        }
    }
    
    private void updateFavContainer(){
        User user = ViewCatsUI.getGlobalUser();
        CatsFav[] cats = CatService.getFavorites();
        
        int newLength = 0;
        for (int i = 0; i < cats.length; i++) {
            try{
                int subId = Integer.parseInt(cats[i].getSub_id());
                if (subId == user.getUser_id()){
                    newLength += 1;
                    System.out.println("Intento de imcremento" + newLength);
                }
            }catch(NumberFormatException e){
                System.out.println(e.getMessage());
            }
            
        }
        if(newLength != 0){
            CatsFav[] newCats = new CatsFav[newLength];
            int j = 0;
            for (int i = 0; i < cats.length; i++) {
                try{
                    int subId = Integer.parseInt(cats[i].getSub_id());
                    if(subId == user.getUser_id()){
                        newCats[j] = cats[i];
                        j++;
                    }
                }catch(NumberFormatException e){
                    System.out.println(e.getMessage());
                }

            }
            this.cat = newCats;
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel catImage;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JPanel panelImage;
    // End of variables declaration//GEN-END:variables
    private int posImage = 0;
    private CatsFav[] cat;
}
