package database;

import io.github.cdimascio.dotenv.Dotenv;
import java.sql.DriverManager;
import java.sql.Connection;

public class DbConnection {
    public static Connection get_Connection(){
        Connection connection = null;
        Dotenv dotenv = Dotenv.load();
        
        try{
            connection = DriverManager.getConnection("jdbc:mysql://"
            + dotenv.get("DATABASE_HOST") +  
                   ":" + dotenv.get("DATABASE_PORT") +
                    "/" + dotenv.get("DATABASE_NAME") +
                    "",  dotenv.get("DATABASE_USER"),
                      dotenv.get("DATABASE_PASSWORD"));
            System.out.println("Successful connection.");

        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return connection;
    }
    
    public static void main(String[] args) {
        DbConnection connection = new DbConnection();
        connection.get_Connection();
    }
}
